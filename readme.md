
## Maven pom styled

This repository has an xml style sheet, specific for maven pom xml, to apply a dark theme and naively turn various sections into useful html. In this fashion, when looking at a pom that uses this stylesheet, presumably on maven central, one isn't obliged to stare at the default light theming that most (all?) browsers use.

This xslt file is &copy; Nicholas Prado, released under the Blue oak council license terms v1.

### current state

Suitable for the example pom, though I basically threw up my hands for the build plugin block, as I may need to provide individual style sub-templates for each of the major plugins.

Unfortunately, on an actual pom (the base for the example) with different ordering, the browsers I've tried time out or something. So, not yet fit for the intended purpose.

