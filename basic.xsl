<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="project">
		<html language='en-us'>
			<head>
				<meta charset="utf-8" />
				<title>
					Maven pomfor <xsl:value-of select="name"/>
					</title>
				<meta>
					<xsl:attribute name="name">
						description
						</xsl:attribute>
					<xsl:attribute name="content">
						maven pom for <xsl:value-of select="name"/>
						</xsl:attribute>
					</meta>
				<style type="text/css">
					body
					{
						background-color:Navy;
						color: Silver;
					}
					.modelVersion
					{
						display:block;
						font-weight:bold;
					}
					</style>
				</head>
			<body>
				<h2>
					Maven pom for <xsl:value-of select="name"/>
					</h2>
				<p>
					<span>
						Maven model - <xsl:value-of select="modelVersion"/>
						</span>
					</p>
				<p>
					Artifact - <xsl:value-of select="groupId"/> <xsl:value-of select="artifactId"/> <xsl:value-of select="version"/>
					</p>
				<p>
					Desc - <xsl:value-of select="description"/>
					<br/>
					Started <xsl:value-of select="inceptionYear"/>
					</p>
				<xsl:apply-templates/>
				</body>
			</html>
		</xsl:template>

	<xsl:template match="licenses">
		<p>
		<xsl:for-each select="license">
			<span>
				License - <xsl:value-of select="name"/>
				<a>
					<xsl:attribute name="href">
						<xsl:value-of select="url"/>
						</xsl:attribute>
					view here
					</a>
				</span>
			</xsl:for-each>
			</p>
		</xsl:template>

	<xsl:template match="dependencies">
		<p>
			Dependencies
			<ul>
				<xsl:for-each select="dependency">
					<li>
						(g)<xsl:value-of select="groupId"/>
						(a)<xsl:value-of select="artifactId"/>
						(v)<xsl:value-of select="version"/>
						<xsl:if test="scope">
							(scope - <xsl:value-of select="scope"/>)
							</xsl:if>
						</li>
					</xsl:for-each>
				</ul>
			</p>
		</xsl:template>

	<xsl:template match="developers">
		<p>
			Developers
			<ul>
				<xsl:for-each select="developer">
					<li>
						<xsl:value-of select="name"/>,
						aka <xsl:value-of select="id"/>,
						<xsl:value-of select="email"/>
						<xsl:if test="timezone">
							(timezone <xsl:value-of select="timezone"/>)
							</xsl:if>
					<xsl:for-each select="roles">
						<xsl:value-of select="role"/>
						</xsl:for-each>
						</li>
					</xsl:for-each>
				</ul>
			</p>
		</xsl:template>

	<xsl:template match="scm">
		<p>
			Scm - url <xsl:value-of select="url"/>
			via <xsl:value-of select="connection"/>
			</p>
		</xsl:template>

	<xsl:template match="distributionManagement">
		<p>
			<xsl:if test="repository">
				repository
				<xsl:value-of select="repository/id"/>
				<xsl:value-of select="repository/url"/>
				</xsl:if>
			<br/>
			<xsl:if test="snapshotRepository">
				snapshots
				<xsl:value-of select="snapshotRepository/id"/>
				<xsl:value-of select="snapshotRepository/url"/>
				</xsl:if>
			</p>
		</xsl:template>

	<xsl:template match="build/plugins">
		<p>
			Build plugins
			<ul>
				<xsl:for-each select="plugin">
					<li>
						(g)<xsl:value-of select="groupId"/>
						(a)<xsl:value-of select="artifactId"/>
						(v)<xsl:value-of select="version"/>
						<xsl:if test="configuration">
							<br/>
							config:
							<xsl:value-of select="configuration"/>
							</xsl:if>
						<xsl:value-of select="configuration"/>
						<xsl:for-each select="executions/execution">
							<xsl:if test="id">
								id-
								<xsl:value-of select="id"/>
								</xsl:if>
							<xsl:if test="phase">
								phase-
								<xsl:value-of select="phase"/>
								</xsl:if>
							<xsl:for-each select="goals">
								goals
								<xsl:value-of select="goal"/>
								</xsl:for-each>
							</xsl:for-each>
						</li>
					</xsl:for-each>
				</ul>
			</p>
		</xsl:template>

	</xsl:stylesheet>

